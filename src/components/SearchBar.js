import React from 'react'

class SearchBar extends React.Component {
  state = { temp: '' }

  onInputChange = e => {
    this.setState({ temp: e.target.value })
  }

  onFormSubmit = e => {
    e.preventDefault();
    this.props.onFormSubmit(this.state.temp);
  }

  render() {
    return <div className='search-bar ui segment'>
      <form className='ui form' onSubmit={this.onFormSubmit}>
        <div className='field'>
          <label>Video Search</label>
          <input
            type='text'
            value={this.state.temp}
            onChange={this.onInputChange}
          />
        </div>
      </form>
    </div>
  }
}

export default SearchBar;