import React, { Fragment } from 'react';
import SearchBar from './SearchBar';
import VideoList from './VideoList';
import youtube, { KEY } from './../apis/youtube';
import VideoDetail from './VideoDetail';
import Spinner from './Spinner';

class App extends React.Component {
  state = { videos: [], selectedVideo: null, loading: false };

  componentDidMount = () => {
    this.onTermSubmit('vietnam');
  }

  onTermSubmit = async (term) => {
    this.setState({ loading: true })
    const response = await youtube.get('search', {
      params: {
        q: term,
        part: 'snippet',
        maxResults: 5,
        type: 'video',
        key: KEY,
      }
    });
    this.setState({ videos: response.data.items, selectedVideo: response.data.items[0], loading: false });
  }

  onVideoSelect = (video) => {
    this.setState({ selectedVideo: video })
  }

  renderConent = () => {
    if (this.state.loading) {
      return <Spinner />
    }
    return (
      <div className='ui container'>
        <SearchBar onFormSubmit={this.onTermSubmit} />
        <div className='ui grid'>
          <div className='ui row'>
            <div className='eleven wide column'>
              <VideoDetail video={this.state.selectedVideo} />
            </div>
            <div className='five wide column'>
              <VideoList onVideoSelect={this.onVideoSelect} videos={this.state.videos} />
            </div>
          </div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <Fragment>{this.renderConent()}</Fragment>
    )
  };
};

export default App;